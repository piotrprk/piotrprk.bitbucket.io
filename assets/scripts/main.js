(function($, document, window){
	
	$(document).ready(function(){

		// Cloning main navigation for mobile menu
		$(".mobile-navigation").append($(".main-navigation .menu").clone());

		// Mobile menu toggle 
		$(".menu-toggle").click(function(){
			$(".mobile-navigation").slideToggle();
		});
        
        /* 
        *   weather forecast section
        *
        */

        // fetch data and cache
        // options = object {expiry : 'seconds'}
        // default expiry time 10 minutes
        const cacheFetch = (url, options = {expiry: 10*60}) => {
            const expiry = options.expiry;
            // make a kay to identyfy the cached url
            // base64 encode
            const cache_key = btoa(url);
            // check if the url is already cached
            let cached = localStorage.getItem(cache_key);
            let response; 
            if (cached){
                // check cache validity
                let when_cached = localStorage.getItem(cache_key + ':timestamp');       
                let age = (Date.now() - when_cached) / 1000;    
                if (age < expiry){
                    response = new Response(new Blob([cached]));                   
                } else {
                    // clean the outdated cache
                    localStorage.removeItem(cache_key);
                    localStorage.removeItem(cache_key+':ts');
                    response = fetchData(url);
                }
            } else {
                response = fetchData(url);                
            }         
            return Promise.resolve(response);
        };
        // get Json data from url
        // save data to local storage
        const fetchData = (url) =>{
            let cache_key = btoa(url);
            const fetch_o = fetch(url)
                .then(response => {
                    if (response.ok){ 
                            response.clone().text()
                            .then(json => {
                                try{
                                    localStorage.setItem(cache_key, json);
                                    localStorage.setItem(cache_key+':timestamp', Date.now());                                    
                                } catch (error){
                                    console.error(error);   
                                }                      
                            });      
                    return response;  
                    }                
                    throw new Error('Error! Network problem');
                    
                })
                .catch(error => {
                    console.error('there was an error fetching data:', error);
                });
                return fetch_o;            
        };
        
    
        // set the html objects
        // weather - weather json branch as in the openWeather

        const today_template = (weather) => {
            
            const today_weather = {
                temperature : {
                    class: '.degree',
                    value: weather.main.temp,
                    template: function(){
                        return `${Math.round(this.value)} &deg;C`;
                    }
                },    
                temp_minmax : {
                    class: '.temp-min-max .num',
                    value: { 0: weather.main.temp_min, 1: weather.main.temp_max },
                    template: function(){
                        return `min/max: ${Math.round(this.value[0])} &deg;C / ${Math.round(this.value[1])} &deg;C`;
                    }               
                },
                temp_min : {
                    class: '.temp-min',
                    value: weather.main.temp_min,
                    template: function(){
                        return `${Math.round(this.value)} &deg;C`;
                    }               
                },
                
                pressure : {
                    class: '.pressure',
                    value: weather.main.pressure,
                    template: function(){
                        return `${Math.round(this.value)} hPa`;
                    }
                },
                humidity : {
                    class: '.humidity .num',
                    value: weather.main.humidity,
                    template: function(){
                        return `${Math.round(this.value)} %`;
                    }
                },
                wind_speed : {
                    class: '.wind-speed .num',
                    value: weather.wind.speed,
                    template: function(){
                        return `${Math.round(this.value)} m/s`;
                    }
                },   
                wind_dir : {
                    class: '.wind-direction .num',
                    value: weather.wind.deg,
                    template: function(){
                        return `${Math.round(this.value)} &deg;`;
                    }
                },      
                rain : {
                    class: '.rain-volume',
                    value: (weather.rain) ? weather.rain['3h'] : 0,
                    template: function(){
                        return `${Math.round(this.value)} mm`;
                    }
                }, 
                icon : {
                    class: '.forecast-icon',
                    value: [ weather.weather[0].icon, weather.weather[0].description], 
                    template: function(){
                        return `<img src="https://openweathermap.org/img/w/${this.value[0]}.png" alt="${this.value[1]}"><div class="description">${this.value[1]}</div>`;
                    }                    
                },
                day : {
                    class: '.day',
                    value: moment(weather.dt, 'X').utc().format('dddd'),
                    template: function(){
                        return `${this.value}`;
                    }
                }, 
                date : {
                    class: '.date',
                    value: moment(weather.dt, 'X').utc().format('Do MMM, HH:mm'),
                    template: function(){
                        return `${this.value}`;
                    }
                },
                hour : {
                    class: '.hour',
                    value: moment(weather.dt, 'X').utc().format('HH:mm'),
                    template: function(){
                        return `at ${this.value}`;
                    }
                }                    
            };
            return today_weather;
        };
        

        // set the html values
        // use special day template object
        // day : { class, value, teplate()}
        // template() uses the value to output html object's values
        // dom_object - jquery parent object        
        const set_html = (day, dom_object) => {
            // traverse throug object and set the values
            for(let key in day){                
                    dom_object.find(day[key].class).html(day[key].template());      
            }    
        };
        
        // default place's settings
        const settings ={
            appID : "136dbb4e26a2ecb306252384d0b59da8",
            city : "warsaw",
            country_code : "pl",
            city_id : '756135',
            type : "forecast",
            //what data to retrive
            // weather - current weather data
            // forecast - 5 days forecast
            url : function () {
                return `https://api.openweathermap.org/data/2.5/${this.type}?q=${this.city},${this.country_code}&units=metric&APPID=${this.appID}`;               
                },
            // url to get forecast with given city id
            url_id : function() {
                return `https://api.openweathermap.org/data/2.5/${this.type}?id=${this.city_id}&units=metric&APPID=${this.appID}`;
            },
            more_cities: {
                'New York': '5128581',
                'Bangalore': '1277333',
                'Paris': '2988507',
                'Tokyo': '1850147'
            },
            more_cities_url: function() {
                let cities_ids = Object.values(this.more_cities).toString();                
                return `https://api.openweathermap.org/data/2.5/group?id=${cities_ids}&units=metric&APPID=${this.appID}`;    
            }
            
        };
        
        // retrive next 'num_days' weather forecast (default = 5)
        // at 'at_hour'
        // from OpenWeather JSON list        
        const get_next_days_forecast = (list, num_days = 5, at_hour = 12) => {
                let days = {};
                for (let i = 0; i < num_days; i++ ){
                    let next_day_at_hour = moment(list[0].dt, 'X').utc().add(i+1, 'd').set({hours:at_hour, minutes: '00', seconds: '00'}).format('X');
                    let j = 1;
                    while (j < list.length){                            
                        let dt = list[j].dt;
                        if (next_day_at_hour == dt) {                                
                            break;
                        }
                        j++;
                    }
                    if (list[j])
                        days[i] = list[j];                        
                }
                // if did't get the fifth day forecast get the last one from the last list entry
                let lost_day = num_days - Object.keys(days).length;
                if (lost_day > 0) {
                    let j = 1;
                    for (let i=num_days-lost_day; i<num_days; i++){                        
                        days[i] = list[list.length - j++];    
                    }                    
                }   
                return days;
        };
        
        // get cities weather data from openWeather json
        const get_cities = (list, how_many) => {
            let cities = {};
            for (let i = 0; i < how_many; i++ ){  
                if (list[i])
                        cities[i] = list[i];      
            }
            return cities;
        };
        // set city name and id to html object
        const set_city_name_id = (city_name, city_id, object) => {            
                object.find('.location').text(city_name).attr('data-id', city_id);               
        };
        
        // fetch weather data and set html object's values
        const fetch_and_set_forecast = (settings) =>{
             cacheFetch(settings.url_id())
                .then(result => {
                    if (result)
                        return result.json();
                })
                .then(info => {                   
                    //console.log(info);
                 
                    // display location name
                    $('#forecast').find('.location').text(info.city.name);
                 
                    // display today's forecast
                    let today = today_template(info.list[0]);
                    set_html (today, $('.today.forecast'));
                 
                    //get the next 5 days forecast
                    let five_days = get_next_days_forecast(info.list);                                        
                 
                    // display 5 days forecast with template
                    for (let i = 0; i < Object.keys(five_days).length; i++){
                        let obj_name = `.forecast.day-${i+1}`;
                        let day = today_template(five_days[i]);
                        set_html (day, $(obj_name));    
                    }                                     
                    
                });      
                return true;
        };
        // fetch weather data and set html object's values
        const fetch_and_set_cities = (settings) => {
                // fetch more cities
                cacheFetch(settings.more_cities_url())
                .then(result => {
                    if (result)
                        return result.json();
                })
                .then(info => {                   
                    //console.log(info);
                    
                    //display cities weather
                    let cities_weather = get_cities(info.list, Object.keys(settings.more_cities).length);
                    for (let i = 0; i < Object.keys(cities_weather).length; i++){
                        let obj_name = `.forecast.cities-${i+1}`;
                        let city = today_template(cities_weather[i]);
                        set_html (city, $(obj_name)); 
                        set_city_name_id (info.list[i].name, info.list[i].id, $(obj_name));
                    }
                });
                return true;
        };
        
        // do this on start
        fetch_and_set_forecast(settings);
        fetch_and_set_cities(settings);
        
        $('#cities .forecast a, #default-city > a').on('click', function(){
            let id = $(this).parent().find('.location').attr('data-id');
            let city_settings = new Object(settings);
            city_settings.city_id = id;
            fetch_and_set_forecast(city_settings);
            //go to the top of page
            $('html, body').animate({ scrollTop: 0 }, 'fast');
        });
        


	});//end of document.ready



})(jQuery, document, window);