// ## Globals
var autoprefixer = require('gulp-autoprefixer');
var browserSync  = require('browser-sync').create();
var changed      = require('gulp-changed');
var concat       = require('gulp-concat');
var flatten      = require('gulp-flatten');
var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var imagemin     = require('gulp-imagemin');
var jshint       = require('gulp-jshint');
var lazypipe     = require('lazypipe');
var less         = require('gulp-less');
var merge        = require('merge-stream');
var cssNano      = require('gulp-cssnano');
var plumber      = require('gulp-plumber');
var rev          = require('gulp-rev');
var runSequence  = require('run-sequence');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var uglify       = require('gulp-uglify');
var svgmin       = require('gulp-svgmin');
var stylish      = require('jshint-stylish');
//

var globs = {
    // Get all style dependencies from here
    // include bootstrap components in the main.scss
    "cssSources": [
        "assets/fontello/css/fontello-embedded.css",
        "assets/styles/main.scss"
    ],
    // JS scripts sources
    "scriptsSources" : [      
        "assets/scripts/*.js",
        "node_modules/moment/min/moment.min.js"        
    ],
    //fonts
    "fontsSources" : ["assets/fonts/*"],
    //images
    "imagesSources" : ["assets/images/*"],
    //main styles output
    "cssOut" : "styles/main.css",
    //main scripts output
    "scriptsOut" : "scripts/main.js",
    //browser sync local URL
    "devUrl": "http://127.0.0.1/weather-4wipro"
};

//paths
var path ={
    "source": "assets/",
    "dist": "dist/"
};

// Error checking; produce an error rather than crashing.
var onError = function(err) {
  console.log(err.toString());
  this.emit('end');
};

// ## Reusable Pipelines
// See https://github.com/OverZealous/lazypipe

// ### CSS processing pipeline
// Example
// ```
// gulp.src(cssFiles)
//   .pipe(cssTasks('main.css')
//   .pipe(gulp.dest(path.dist + 'styles'))
// ```
var cssTasks = function(filename) {
  return lazypipe()
    .pipe(function() {
      return plumber();
    }) 
    .pipe(function() {
      return gulpif('*.less', less());
    })
    .pipe(function() {
      return gulpif('*.scss', sass({
        outputStyle: 'nested', // libsass doesn't support expanded yet
        precision: 10,
        includePaths: ['.'],
        errLogToConsole: true
      }));
    })
    .pipe(concat, filename)
    .pipe(autoprefixer, {
      browsers: [
        'last 2 versions',
        'android 4',
        'opera 12'
      ]
    })
    .pipe(cssNano, {
      safe: true
    })();
};

// ### JS processing pipeline
// Example
// ```
// gulp.src(jsFiles)
//   .pipe(jsTasks('main.js')
//   .pipe(gulp.dest(path.dist + 'scripts'))
// ```
var jsTasks = function(filename) {
  return lazypipe()
    .pipe(concat, filename)
 /*
 .pipe(uglify, {
      compress: {
        'drop_debugger': false
      }
    })
    */
    ();
};

// ### Styles
// `gulp styles` - Compiles, combines, and optimizes CSS.
gulp.task('styles', function() {
    gulp.src(globs.cssSources)
    .pipe(plumber({errorHandler: onError}))
    .pipe(cssTasks(globs.cssOut))
    .pipe(gulp.dest(path.dist));
});

// ### Scripts
// `gulp scripts` - Runs JSHint then compiles, combines, and optimizes Bower JS
// and project JS.
gulp.task('scripts', ['jshint'], function() {
      gulp.src(globs.scriptsSources)
        .pipe(plumber({errorHandler: onError}))
        .pipe(jsTasks(globs.scriptsOut))
        .pipe(gulp.dest(path.dist));
});

// ### Fonts
// `gulp fonts` - Grabs all the fonts and outputs them in a flattened directory
// structure. See: https://github.com/armed/gulp-flatten
gulp.task('fonts', function() {
  return gulp.src(globs.fontsSources)
    .pipe(flatten())
    .pipe(gulp.dest(path.dist + 'fonts'))
    .pipe(browserSync.stream());
});

// ### Images
// `gulp images` - Run lossless compression on all the images.
gulp.task('images', ['svgs'], function() {
  return gulp.src(globs.imagesSources)
    .pipe(imagemin([
      imagemin.jpegtran({progressive: true}),
      imagemin.gifsicle({interlaced: true}),
      imagemin.svgo({plugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]})
    ]))    
    .pipe(gulp.dest(path.dist + 'images'))
    .pipe(browserSync.stream());
});

// ### SVG's
// `svgmin` - minimize svg's.
gulp.task('svgs', function() {
  return gulp.src(globs.imagesSources + '/*.svg')
    .pipe(svgmin())
    .pipe(gulp.dest(path.dist + 'images'))
    .pipe(browserSync.stream());
});

// ### JSHint
// `gulp jshint` - Lints configuration JSON and project JS.
gulp.task('jshint', function() {
  return gulp.src([
    'package.json', 'gulpfile.js'
  ].concat(globs.scriptsSources)
    .concat('!node_modules/moment/min/moment.min.js'))
    .pipe(jshint({
      esversion: 6
    } ))
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    ;
});

// ### Clean
// `gulp clean` - Deletes the build folder entirely.
gulp.task('clean', require('del').bind(null, [path.dist]));

// ### Watch
// `gulp watch` - Use BrowserSync to proxy your dev server and synchronize code
// changes across devices. Specify the hostname of your dev server at
// `manifest.config.devUrl`. When a modification is made to an asset, run the
// build step for that asset and inject the changes into the page.
// See: http://www.browsersync.io
gulp.task('watch', function() {
  browserSync.init({
    files: ['*.php', '*.htm', '*.html', 'dist/**/*'],
    proxy: globs.devUrl,
    snippetOptions: {
      whitelist: [],
      blacklist: []
    }
  });
  gulp.watch([path.source + 'styles/**/*'], ['styles']);
  gulp.watch([path.source + 'scripts/**/*'], ['jshint', 'scripts']);
  gulp.watch([path.source + 'fonts/**/*'], ['fonts']);
  gulp.watch([path.source + 'images/**/*'], ['images', 'svgs']);
  gulp.watch(['package.json'], ['build']);
});

// ### Build
// `gulp build` - Run all the build tasks but don't clean up beforehand.
// Generally you should be running `gulp` instead of `gulp build`.
gulp.task('build', function(callback) {
  runSequence('styles',
              'scripts',
              ['fonts', 'images'],
              callback);
});

// ### Gulp
// `gulp` - Run a complete build. To compile for production run `gulp --production`.
gulp.task('default', ['clean'], function() {
  gulp.start('build');
});