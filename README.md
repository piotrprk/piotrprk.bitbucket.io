# Weather forecast for buildit @ wipro digital

This is a test project.
Retrieve weather forecast from OpenWeatherMap API and display on a single webpage.

## Features
* Sass for stylesheets
* Modern JavaScript
* JQuery/CSS framework: [Bootstrap 4](https://getbootstrap.com/), 
* [Animate.css] (https://daneden.github.io/animate.css/) for nice and neat object fade-in,
* [Moments.js](https://momentjs.com/) for date-time manipulations,
* [Fontello] (http://fontello.com/) font icons embedded into CSS for speed and lightness,
* Automatic building with [Node.js]I(https://nodejs.org) / [Yarn](https://yarnpkg.com/en/) and [Gulp](https://gulpjs.com/),
* Build with [JsHint](https://jshint.com/) for code quality,
* Build with [CSS Autoprefixer] (https://github.com/postcss/autoprefixer) to ensure styles compatibility with all browsers
See a working example at [weather-4wipro live page](https://piotrprk.bitbucket.io/)
##Requirements

It works out of the box. Just open index.htm file in the browser.

To start a working environment you need
* [Node.js](http://nodejs.org/) >= 6.9.x
* [Yarn](https://yarnpkg.com/en/docs/install)

## Installation

Git clone project files to your directory.

```shell
# @ weather-4wipro/ 
$ git clone git@bitbucket.org:piotrprk/piotrprk.bitbucket.io.git
```


Install a working environment dependencies with:

```shell
# @ weather-4wipro/ 
$ yarn install
```

## Setup

(optional) Edit Gulpfile.js to change your local server address to use Browser-sync feature:

````shell
    //browser sync local URL
    "devUrl": "http://127.0.0.1/LOCAL_DIR_NAME"
````

## Development

Compile project with

````shell
# @ weather-4wipro/ 
$ gulp
````
to use live changes update and browser sync:

````shell
# @ weather-4wipro/ 
$ gulp watch
````

## Deployment

To deploy the project Git clone the files to a working live server.

## Versioning

[Bitbucket.org] (https://bitbucket.org/piotrprk/piotrprk.bitbucket.io/)

## Trade offs 

* Because of the small time frame there's one default place/city (Warsaw) at start
* No city/place selector or search box
* Wind direction icon pointing right direction
* Wanted to use fontello weather icons representing the weather state, added them to the project, but used default OpenWeather icons instead

## To Do

* Find the city feature that allows user to choose from cities available in OpenWeatherMap. There's a large JSON with city name - ID pairs. I could do live search (google style) with JS/Ajax inside the data file.
* wind direction with icon pointing right direction, not the raw data as it is now 
* use custom icons representing the weather state
* Localization l10n
* Internationalization i18n
* JS html templating with lit-html or other framework
* Add live weather cams
* Add images that represent season (Summer / Autumn ...)

## Authors

* [**Piotr Kaczmarek**](mailto:piotr@visuala.pl)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
